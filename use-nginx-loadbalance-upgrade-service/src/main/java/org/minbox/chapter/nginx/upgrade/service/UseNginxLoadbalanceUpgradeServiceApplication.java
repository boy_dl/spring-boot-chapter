package org.minbox.chapter.nginx.upgrade.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UseNginxLoadbalanceUpgradeServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(UseNginxLoadbalanceUpgradeServiceApplication.class, args);
    }

}
